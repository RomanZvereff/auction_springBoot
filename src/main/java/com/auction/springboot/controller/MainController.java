package com.auction.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {

    @RequestMapping("/")
    public String getIndexPage(){
        return "index";
    }

    @RequestMapping("/seller")
    public String getSellerPage() {
        return "seller";
    }

    @RequestMapping("/product")
    public String getProductPage() {
        return "product";
    }

    @RequestMapping("/buyer")
    public String getBuyerPage() {
        return "buyer";
    }

    @RequestMapping("/bid")
    public String getBidPage() {
        return "bid";
    }



}
