package com.auction.springboot.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter                                //Если @Accessors установлен в true, то сгенерированные методы установки
@Setter                                //значения возвращают this (вместо void).
@Accessors(chain = true)
public class BidDTO {

    private int bid_size;
    private int fk_product_id;
    private int fk_buyer_id;
}
