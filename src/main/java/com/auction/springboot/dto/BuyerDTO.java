package com.auction.springboot.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter                                //Если @Accessors установлен в true, то сгенерированные методы установки
@Setter                                //значения возвращают this (вместо void).
@Accessors(chain = true)
public class BuyerDTO {

    private String buyer_first_name;
    private String buyer_last_name;
    private String buyer_phone;
    private int buyer_bid;

}
