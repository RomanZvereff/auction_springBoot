package com.auction.springboot.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter                                //Если @Accessors установлен в true, то сгенерированные методы установки
@Setter                                //значения возвращают this (вместо void).
@Accessors(chain = true)
public class ProductDTO {

    private String product_name;
    private String product_type;
    private String product_address;
    private int product_start_price;
    private String product_date_of_sale;
    private String product_status;
    private int fk_seller_id;

}
