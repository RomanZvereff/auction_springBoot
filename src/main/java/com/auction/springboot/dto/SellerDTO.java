package com.auction.springboot.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter                                //Если @Accessors установлен в true, то сгенерированные методы установки
@Setter                                //значения возвращают this (вместо void).
@Accessors(chain = true)
public class SellerDTO {

    private String seller_first_name;
    private String seller_last_name;
    private String seller_phone_number;

}
